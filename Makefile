#!/usr/bin/make -f
#

### CONFIGURATION BEGINS ###
PACKAGE:=puppet-rock
MAINTAINER:=<derek@criticalstack.com>
VENDOR:=Critical Stack, Inc.
WEBSITE:=http://www.criticalstack.com
PACKAGE:=puppet-rock
DESCRIPTION:=Build scripts for Critical Stack sensor configurations.
VERSION:=0.2.0
RELEASE:=1
ARCH:=all

INSTALLDIR:=/usr/share/puppet/modules

### CONFIGURATION ENDS ###

rpm:
	fpm -t rpm -s dir -C modules \
                 --iteration "${RELEASE}" \
                 --license BSD \
                 --maintainer "${MAINTAINER}" \
                 --description "${DESCRIPTION}" \
                 --vendor "${VENDOR}" \
                 --url "${WEBSITE}" \
                 --prefix "${INSTALLDIR}" \
                 --exclude ".git" \
                 -n "${PACKAGE}" \
                 -v "${VERSION}" \
                 -a "${ARCH}"

