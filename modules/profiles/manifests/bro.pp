# Depends on these modules currently:
# - panaman-bro
#
class profiles::bro {

  ## Hiera lookups
  $site_name               = hiera('profiles::site::site_name')
  $sensor_name             = hiera('profiles::sensor::sensor_name')
  
  # trusted_net defaults to the local management network, as determined by facter
  # All services, will by default accept connections from this network. Further refinement
  # can be performed using the service specific values below.
  $trusted_net             = hiera('profiles::sensor::trusted_net',  "$::ipaddress/$::netmask")
  $trusted_ssh             = hiera('profiles::sensor::trusted_ssh',  "$trusted_net")
  $trusted_http            = hiera('profiles::sensor::trusted_http', "$trusted_net")

  $monitor_if              = hiera('profiles::sensor::monitor_if')
  $monitor_nets            = hiera('profiles::sensor::monitor_nets', ['10.0.0.0/8','172.16.0.0/12','192.168.0.0/16'])

  class { '::bro':
    int => $monitor_if,
    network => $monitor_nets,
  }
}

