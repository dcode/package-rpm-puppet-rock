# == Class: profile::apache
#
# Apache profile
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::apache
#
# === Authors
#
# Rob Nelson <rnelson0@gmail.com>
#
# === Copyright
#
# Copyright 2014 Rob Nelson
#
class profiles::kibana {
  nginx::resource::vhost { "$::hostname":
    proxy => 'http://localhost:5601',
    proxy_redirect => "http://localhost:5601 http://$sensor_name",
  }
}
