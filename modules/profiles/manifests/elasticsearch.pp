# Depends on these modules currently:
# - elasticsearch-elasticsearch
#
class profiles::elasticsearch {

  ## Hiera lookups
  $rock_esinstance         = hiera('profiles::sensor::esinstance',   "rock")

  class { '::elasticsearch':
    autoupgrade => false,
    manage_repo => true,
    repo_version => 1.4,
  }

  elasticsearch::instance { "$rock_esinstance": }

  # Firewall config would go here, but we don't want direct ES access, could optionally proxy it too
}

